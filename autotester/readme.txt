autotester是一款为API测试人员打造的自动化测试系统。
测试版1.0提供了case录入以及定时自动化测试的功能。
项目环境依赖：
1.Python2.6
2.PyCurl7.19.0
3.Django1.4.15
4.目前可以在CentOS6.5上正常运行
5.celery 3.1.16
6.rabbitMQ

初步测试项目部署环境可为apache+mod_wsgi
对于apache的配置可参考doc目录下的httpd.conf以及wsgi.conf

部署步骤：
安装apache:yum install httpd-devel
安装mod_wsgi:下载mod_wsgi https://github.com/GrahamDumpleton/mod_wsgi/archive/3.3.zip(注意版本)
将项目源码拖到/var/www/下，并将httpd.conf以及wsgi.conf拖到/etc/httpd/conf和conf.d下
配置settings.py中的database

cd到/var/www/autotester执行：
env/bin/python manage.py sqlall
env/bin/python manage.py syncdb

按步骤操作
启动apache；/etc/rc.d/init.d/httpd start

使用自动化定时测试需启动心跳即运行startbeat.sh

